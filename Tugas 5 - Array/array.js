/*Tugas 5-Array*/
//Nomor 1 - Range
console.log('-----------------------1-----------------------');
function range(startNum, finishNum) {
    var OutList = [];
    if (startNum < finishNum) {
        var i = startNum;
        while (i <=finishNum) {
            OutList.push(i);
            i ++;
        }
        return OutList;
    }else if (startNum > finishNum) {
        var i = startNum;
        while (i >=finishNum) {
            OutList.push(i);
            i --;
        }
        return OutList;
    }else{
        return -1;
    }
}
//TestCase
console.log(range(1, 10)); //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)); // -1
console.log(range(11,18)); // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)); // [54, 53, 52, 51, 50]
console.log(range()); // -1 
console.log('-----------------------------------------------');
//Nomor 2 - Range with Step
console.log('-----------------------2-----------------------');
function rangeWithStep(startNum, finishNum, step) {
    var OutList = [];
    if (startNum < finishNum) {
        var i = startNum;
        while (i <=finishNum) {
            OutList.push(i);
            i += step;
        }
        return OutList;
    }else if (startNum > finishNum) {
        var i = startNum;
        while (i >=finishNum) {
            OutList.push(i);
            i -= step;
        }
        return OutList;
    }else{
        return -1;
    }
}
//TestCase
console.log(rangeWithStep(1, 10, 2)); // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)); // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)); // [29, 25, 21, 17, 13, 9, 5] 
console.log('-----------------------------------------------');
//Nomor 3 - Sum of Range
console.log('-----------------------3-----------------------');
function sum(startNum, finishNum, step) {
    var total = 0;
    if ((finishNum == undefined) && (startNum != undefined)) {
        return startNum;
    }if (startNum < finishNum) {
        var i = startNum;
        while (i <= finishNum) {
            total +=i;
            if (step == undefined) {
                i ++;
            }else{
                i += step;
            }
        }
        return total;
    }else if (startNum > finishNum) {
        var i = startNum;
        while (i >=finishNum) {
            total +=i;
            if (step == undefined) {
                i --;
            }else{
                i -= step;
            }
        }
        return total;
    }else{
        return 0;
    }
}
//TestCase
console.log(sum(1,10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15,10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)); // 1
console.log(sum()); // 0
console.log('-----------------------------------------------');
//Nomor 4 - Array Multidimensi
console.log('-----------------------4-----------------------');
//contoh input
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];
function dataHandling (data){
    for (var index = 0; index < data.length; index++) {
        for (var indexdata = 0;indexdata < data[index].length; indexdata++) {
            if(indexdata == 0){
                console.log("Nomor ID: "+ data[index][indexdata]);
            }else if(indexdata == 1){
                console.log("Nama Lengkap: "+data[index][indexdata]);
            }else if(indexdata == 2){
                console.log("TTL: "+data[index][indexdata]+" "+data[index][3]);
            }else if(indexdata == 4){
                console.log("Hobi: "+data[index][indexdata]);
            }
        }console.log("");
    }
}
dataHandling(input);
console.log('-----------------------------------------------');
//Nomor 5 - Balik Kata
console.log('-----------------------5-----------------------');
function balikKata(kalimat){
    var temp= '';
    for (var index = kalimat.length; index > 0; index--) {
        temp = temp+kalimat[index-1];
    }
    return temp;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I
console.log('-----------------------------------------------');
//Nomor 6 - Metode Array
console.log('-----------------------6-----------------------');
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
function dataHandling2(data){
    data.splice(1,1,"Roman Alamsyah Elsharawy");
    data.splice(2,1,"Provinsi Bandar Lampung");
    data.splice(4,1,"Pria","SMA Internasional Metro");
    console.log(data);
    var temp= data[3];
    var waktu1 = temp.split("/");

    switch (parseInt(waktu1[1])) {
        case 1:{ console.log('Januari');break; }
        case 2:{ console.log('Februari');break; }
        case 3:{ console.log('Maret');break; }
        case 4:{ console.log('April');break; }
        case 5:{ console.log('Mei');break; }
        case 6:{ console.log('Juni');break; }
        case 7:{ console.log('Juli');break; }
        case 8:{ console.log('Agustus');break; }
        case 9:{ console.log('September');break; }
        case 10:{ console.log('Oktober');break; }
        case 11:{ console.log('November');break; }
        case 12:{ console.log('Desember');break; }
        default:{ console.log('Masukkan bulan salah');break; }
    }
    waktu1[0]=parseInt(waktu1[0]);
    waktu1[1]=parseInt(waktu1[1]);
    waktu1[2]=parseInt(waktu1[2]);
    waktu1.sort((a,b)=>b-a);
    waktu1[0]=waktu1[0].toString();
    waktu1[1]=waktu1[1].toString();
    waktu1[2]=waktu1[2].toString();
    console.log(waktu1);
    waktu2 = temp.split("/");
    waktu2 = waktu2.join("-");
    console.log(waktu2);
    console.log(data[1].slice(0,15));
}
dataHandling2(input);