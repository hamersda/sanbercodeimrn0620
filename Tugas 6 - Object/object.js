/*Tugas 6-Object Literak*/
//Nomor 1 - Array to Object
console.log('-----------------------1-----------------------');
var now=new Date()

function arrayToObject(arr) {
    var object1 = {  firstName:"",
                    lastName:"",
                    gender:"",
                    age:""};
    var object2 = {  firstName:"",
                    lastName:"",
                    gender:"",
                    age:""};
    for (let i = 0; i < arr.length; i++) {
        if (i==0) {
            object1.firstName = arr[i][0];
            object1.lastName = arr[i][1];
            object1.gender = arr[i][2];
            if(now.getFullYear()-arr[i][3]>0){
                object1.age = now.getFullYear()-arr[i][3];
            }else{
                object1.age = "Invalid Birth Year";
            }
        }else{
            object2.firstName = arr[i][0];
            object2.lastName = arr[i][1];
            object2.gender = arr[i][2];
            if(now.getFullYear()-arr[i][3]>0){
                object2.age = now.getFullYear()-arr[i][3];
            }else{
                object2.age = "Invalid Birth Year";
            }
        }
    }if (object1.firstName=='') {
        console.log("")
    }else {
        process.stdout.write('1.'+ object1.firstName + ' '+object1.lastName + ":");
        console.log(object1);
        process.stdout.write('2.'+ object2.firstName + ' '+object2.lastName + ":");
        console.log(object2);
    }
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2)  
// Error case 
arrayToObject([]) // ""
console.log('-----------------------------------------------');
//Nomor 2 - Shopping Time
console.log('-----------------------2-----------------------');
function shoppingTime(memberId, money) {
    changeMoney = money;
    var belanja = { memberId:"",
                    money:"",
                    listPurchased:[],
                    changeMoney:""};
    if((memberId==undefined)||(memberId=='')){
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    }else if (money<50000) {
        return "Mohon maaf, uang tidak cukup "
    }else{
        belanja.memberId = memberId;
        belanja.money = money;
        /*Sepatu brand Stacattu seharga 1.500.000
        Baju brand Zoro seharga 500.000
        Baju brand H&N seharga 250.000
        Sweater brand Uniklooh seharga 175.000
        Casing Handphone seharga 50.000*/
        while (changeMoney>=50000) {
            if (changeMoney>=1500000){
                belanja.listPurchased.push("Sepatu Stacattu");
                changeMoney -=1500000;
            }else if (changeMoney>=500000) {
                belanja.listPurchased.push("Baju Zoro");
                changeMoney -=500000;
            }else if (changeMoney>=250000) {
                belanja.listPurchased.push("Baju H&N");
                changeMoney -=250000;
            }else if (changeMoney>=175000) {
                belanja.listPurchased.push("Sweater Uniklooh");
                changeMoney -=175000;
            }else{
                belanja.listPurchased.push("Casing Handphone");
                changeMoney -=50000;
                break;
            }
        }belanja.changeMoney=changeMoney;
        return belanja;
    }
}
   
// TEST CASES
    console.log(shoppingTime('1820RzKrnWn08', 2475000));
    console.log(shoppingTime('82Ku8Ma742', 170000));
    console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
    console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
    console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var semuaPenumpang = [];
    for (let i = 0; i < arrPenumpang.length; i++) {
        var indeksTujuan= 0
        var indeksAsal= 0
        var objectPenumpang = { penumpang:"",
                            naikDari:"",
                            tujuan:"",
                            bayar:""}
        objectPenumpang.penumpang=arrPenumpang[i][0]
        objectPenumpang.naikDari=arrPenumpang[i][1]
        objectPenumpang.tujuan=arrPenumpang[i][2]
        for (let j = 0; j < rute.length; j++) {
            if(arrPenumpang[i][1]==rute[j]){
                indeksAsal=j;
            }if(arrPenumpang[i][2]==rute[j]){
                indeksTujuan=j;
            }            
        }
        objectPenumpang.bayar = (indeksTujuan-indeksAsal)*2000
        semuaPenumpang.push(objectPenumpang);
    }
    return semuaPenumpang;
}
    
   
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
console.log(naikAngkot([])); //[]