/*Tugas 2B - Conditional*/
//if-else
var nama    = 'Hadim';
var peran   = 'Guard';
var output  = ''; 
if (nama == '' && peran == ''){ // input nama dan peran kosong
    output = 'Nama harus diisi!'
}else if(nama != '' && peran == ''){ //peran kosong
    output = 'Halo ' + nama + ', ' + 'Pilih peranmu untuk memulai game!';
}else if(nama != '' && (peran == 'Penyihir'||peran == 'penyihir')){ //peran penyihir
    output = 'Halo Penyihir ' + nama + ', ' + 'kamu dapat melihat siapa yang menjadi werewolf!';
}else if(nama != '' && (peran == 'Guard'||peran == 'guard')){//peran guard
    output = 'Halo Guard ' + nama + ', ' + 'kamu akan membantu melindungi temanmu dari serangan werewolf.';
}else if(nama != '' && (peran == 'Werewolf'||peran == 'werewolf')){//peran werewolf
    output = 'Halo Werewolf ' + nama + ', ' + 'Kamu akan memakan mangsa setiap malam!';
}else{
    output = 'Peran tidak tersedia'
}
console.log('Selamat datang di Dunia Werewolf, ' + nama );
console.log(output);
console.log("-------------------------------------------------------------------------------------");
//Switch Case
// Asumsi masukkan tanggal dan tahun oleh user selalu benar 
var tanggal=1; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan=10; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun=2000; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)
switch (bulan) {
    case 1:{ console.log(tanggal + ' ' + ' Januari' + ' ' + tahun);break; }
    case 2:{ console.log(tanggal + ' ' + ' Februari' + ' ' + tahun);break; }
    case 3:{ console.log(tanggal + ' ' + ' Maret' + ' ' + tahun);break; }
    case 4:{ console.log(tanggal + ' ' + ' April' + ' ' + tahun);break; }
    case 5:{ console.log(tanggal + ' ' + ' Mei' + ' ' + tahun);break; }
    case 6:{ console.log(tanggal + ' ' + ' Juni' + ' ' + tahun);break; }
    case 7:{ console.log(tanggal + ' ' + ' Juli' + ' ' + tahun);break; }
    case 8:{ console.log(tanggal + ' ' + ' Agustus' + ' ' + tahun);break; }
    case 9:{ console.log(tanggal + ' ' + ' September' + ' ' + tahun);break; }
    case 10:{ console.log(tanggal + ' ' + ' Oktober' + ' ' + tahun);break; }
    case 11:{ console.log(tanggal + ' ' + ' November' + ' ' + tahun);break; }
    case 12:{ console.log(tanggal + ' ' + ' Desember' + ' ' + tahun);break; }
    default:{ console.log('Masukkan bulan salah');break; }
}