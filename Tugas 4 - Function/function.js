/*Tugas 4-Functions*/
//Nomor 1
console.log('-----------------------1-----------------------');
function teriak() {
    var temp="Halo Sanbers!";
    return temp;
}
console.log(teriak());
//Nomor 2
console.log('-----------------------2-----------------------');
function kalikan(x,y) {
    return x*y;
}
var num1 = 12;
var num2 = 4;
var hasilKali = kalikan(num1, num2);
console.log(hasilKali); // 48
//Nomor 3
console.log('-----------------------3-----------------------');
function introduce(nama,umur,alamat,hobby){
    var kalimat = "Nama saya " + nama + ", " + "umur saya " + umur + " tahun, " + "alamat saya di " + alamat + ", dan saya punya hobby yaitu " + hobby +"!";
    return kalimat;
}
var name = "Agus";
var age = 30;
var address = "Jln. Malioboro, Yogyakarta";
var hobby = "Gaming";
 
var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan); // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!" 