/*Tugas 3-Looping*/
//Looping While
console.log('---------------1.Looping While-----------------');
console.log('LOOPING PERTAMA');
var num=2;
while (num<=20) {
    console.log(num+' - I love coding');
    num+=2;
}
console.log('LOOPING KEDUA');
while (num>2) {
    num-=2;
    console.log(num+' - I love coding');
}
console.log('-----------------------------------------------');
//Looping For
console.log('---------------2.Looping For-------------------');
for (var index = 1; index <=20; ++index) {
    if((index%3==0)&&(index%2==1)){//Angka ganjil dan kelipatan 3
        console.log(index + ' - I Love Coding');
    }else if (index%2==1) {//Angka Ganjil
     console.log(index + ' - Santai'); 
    }else{//Angka Genap
        console.log(index + ' - Berkualitas');
    }
}console.log('-----------------------------------------------');
//Persegi Panjang #
console.log('--------------3.Persegi Panjang #--------------');
for (var lebar = 1; lebar <= 4; ++lebar) {
    for (var panjang = 1 ;panjang < 8; ++ panjang) {
        process.stdout.write('#');
    }console.log('#');
}console.log('-----------------------------------------------');
//Membuat tangga
console.log('----------------4.Membuat tangga---------------');
for (var dimensi = 1; dimensi <= 7; ++dimensi){
    for (var alas = 1; alas < dimensi; ++alas) {
        process.stdout.write('#');
    }console.log('#');
}console.log('-----------------------------------------------');
//Membuat Papan Catur
console.log('-------------5.Membuat Papan Catur-------------');
var indikasi=0;//penanda hitam atau putih
for (var samping = 1; samping <= 8; ++samping) {
    for (var bawah = 1; bawah < 8; ++bawah) {
        if (indikasi==0) {//putih
            process.stdout.write(' ');
            indikasi++;
        }else{//hitam
            process.stdout.write('#');
            indikasi=0;
        }
    }if (indikasi==0) {//putih
        console.log(' ');
    }else{//hitam
        console.log('#');
    }
}